import { Component } from '@angular/core';
import { LoginService } from '../auth/service/login.service';
import { ContaService } from '../contas/service/conta-service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  date = new Date().toISOString();
  
  conta = {
    pagar: {num: 0, valor: 0}, 
    receber: {num: 0, valor: 0}, 
    saldo: {num: 0, valor: 0}
  };

  constructor(
    private contas: ContaService,
    private service: LoginService
  ) {}

  ionViewWillEnter(){
    this.atualizaContas();  
  }

  atualizaContas(){
    this.contas.total('pagar', this.date).subscribe(
      (x: any) => {
        this.conta.pagar = x;
        this.contas.total('receber', this.date).subscribe(
          (y: any) => {
            this.conta.receber = y
            this.atualizarSaldo();
          }
        );
      }
    );
    
  }
  
  atualizarSaldo(){
    const totalDeContas = this.conta.pagar.num + this.conta.receber.num;
    const saldoTotal = this.conta.receber.valor - this.conta.pagar.valor;
    this.conta.saldo.num = totalDeContas; 
    this.conta.saldo.valor = saldoTotal;
  }

  logout(){
    this.service.logout();
  }
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'footer-voltar',
  templateUrl: './footer-voltar.component.html',
  styleUrls: ['./footer-voltar.component.scss'],
})
export class FooterVoltarComponent implements OnInit {
  @Input() page;

  constructor() { }

  ngOnInit() {}

}

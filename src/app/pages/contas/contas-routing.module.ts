import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CadastroPage } from './cadastro/cadastro.page';
import { RelatorioPage } from './relatorio/relatorio.page';
import { FooterVoltarComponent } from '../auth/component/footer-voltar/footer-voltar.component';
import { TopoLoginComponent } from '../auth/component/topo-login/topo-login.component';
import { ListaPage } from './lista/lista.page';

const routes: Routes = [
  {
    path: '', children: [
      { path: 'pagar', component: ListaPage },
      { path: 'receber', component: ListaPage },
      { path: 'cadastro', component: CadastroPage },
      { path: 'relatorio', component: RelatorioPage }  
    ]
  },
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ListaPage,
    CadastroPage,
    RelatorioPage,
    TopoLoginComponent,
    FooterVoltarComponent
  ]
})
export class ContasRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashCardComponent } from './dash-card/dash-card.component';



@NgModule({
  declarations: [
    DashCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DashCardComponent
  ]
})
export class ComponentModule { }

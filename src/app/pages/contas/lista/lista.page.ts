import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  listaContas;
  tipo;

  constructor(
    private nav: NavController,
    private conta: ContaService,
    private alert: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    const url = this.router.url;
    const tipo = url.split('/')[2];
    this.tipo = tipo.charAt(0).toUpperCase() + tipo.slice(1);
    this.conta.lista(tipo).subscribe(result => this.listaContas = result);
  }

  async remove(conta){
    const confirm = await this.alert.create({
      header: 'Remover Conta',
      message: 'Tem certeza que deseja deletar?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      }, {
        text: 'Deletar',
        handler: () => this.conta.remove(conta)
      }]
    });

    confirm.present();
  }

  async edita(conta){
    const confirm = await this.alert.create({
      header: 'Editar Conta',
      inputs: [
        {
          name: 'parceiro',
          value: conta.parceiro,
          placeholder: 'Parceiro Comercial'
        }, 
        {
          name: 'descricao',
          value: conta.descricao,
          placeholder: 'Descrição'
        }, 
        {
          name: 'valor',
          value: conta.valor,
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        }, 
        {
          text: 'Atualizar',
          handler: (data) => {
            const obj = {...conta, ...data};
            this.conta.edita(obj);
          }
        }
      ]
    });

    confirm.present();
  }
}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBHBHVgsRoDYcu9rNJv4nA7HPjfokIYdBA',
    authDomain: 'controle-ifncs.firebaseapp.com',
    databaseURL: 'https://controle-ifncs.firebaseio.com',
    projectId: 'controle-ifncs',
    storageBucket: 'controle-ifncs.appspot.com',
    messagingSenderId: '916894115986',
    appId: '1:916894115986:web:f1f1e253a91b6424d21e4b',
    measurementId: 'G-YE39BGKRYQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
